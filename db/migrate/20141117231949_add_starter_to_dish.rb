class AddStarterToDish < ActiveRecord::Migration
  def change
    change_table :dishes do |t|
      t.boolean :is_starter
    end
  end
end
