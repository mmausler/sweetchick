class ChangeDishes < ActiveRecord::Migration
  def change
    change_table :dishes do |t|
      t.remove :menu_id
    end
  end
end
