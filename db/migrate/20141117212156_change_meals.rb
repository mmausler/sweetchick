class ChangeMeals < ActiveRecord::Migration
  def change
    change_table :meals do |t|
      t.remove :dish_id
    end
  end
end
