class CreatePress < ActiveRecord::Migration
  def change
    create_table :press do |t|
      t.string :title
      t.text :description
      t.text :embed_code
      t.string :url
      t.string :image_uid
      t.date :release_date

      t.timestamps
    end
  end
end
