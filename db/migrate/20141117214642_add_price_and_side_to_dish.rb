class AddPriceAndSideToDish < ActiveRecord::Migration
  def change
    change_table :dishes do |t|
      t.boolean :is_side
      t.float :price
    end
  end
end
