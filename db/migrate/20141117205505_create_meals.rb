class CreateMeals < ActiveRecord::Migration
  def change
    create_table :meals do |t|
      t.string :title
      t.belongs_to :menu
      t.belongs_to :dish
      t.timestamps
    end
  end
end
