class AddImageNameToPress < ActiveRecord::Migration
  def change
    add_column :press, :image_name, :string
  end
end
