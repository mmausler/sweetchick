class AddTitleAndBodyToPages < ActiveRecord::Migration
  def change
    add_column :pages, :title, :string
    add_column :pages, :body, :text
  end
end
