class AddPositionToPress < ActiveRecord::Migration
  def change
    add_column :press, :position, :integer
  end
end
