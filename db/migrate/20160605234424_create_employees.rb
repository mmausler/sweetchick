class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|
      t.string :name
      t.string :position
      t.text :description
      t.string :image_uid
      t.string :image_name

      t.timestamps
    end
  end
end
