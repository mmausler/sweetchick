-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 19, 2014 at 10:55 PM
-- Server version: 10.0.14-MariaDB-log
-- PHP Version: 5.6.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sweetchick_development`
--

-- --------------------------------------------------------

--
-- Table structure for table `dishes`
--

CREATE TABLE IF NOT EXISTS `dishes` (
`id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `meal_id` int(11) DEFAULT NULL,
  `is_side` tinyint(1) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `is_starter` tinyint(1) DEFAULT NULL,
  `position` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dishes`
--

INSERT INTO `dishes` (`id`, `title`, `description`, `created_at`, `updated_at`, `meal_id`, `is_side`, `price`, `is_starter`, `position`) VALUES
(1, 'Vegetarian Meatloaf', 'pomme puree, peas, carrots', '2014-11-18 03:10:01', '2014-11-19 21:08:20', 7, 0, 22, 0, 1),
(2, 'Shrimp and Grits', 'smoked paprika, bell peppers sauce, scallions, cheddar grits, poached egg', '2014-11-18 18:41:08', '2014-11-19 21:56:53', 7, 0, 22, 0, 2),
(3, 'Chicken Pot Pie', 'poached chicken, potatoes, peas, carrots', '2014-11-18 18:41:40', '2014-11-18 19:29:02', 7, 0, 16, 0, 3),
(4, 'Grilled Skirt Steak', 'bacon and cheddar spoon bread, sautéed baby kale, ancho mustard sauce', '2014-11-18 18:43:17', '2014-11-18 19:28:50', 7, 0, 26, 0, 4),
(5, 'Grilled Berkshire Pork Chop', 'butternut squash, yogurt puree, beluga lentils, pomegranate glaze', '2014-11-18 18:43:51', '2014-11-18 19:28:58', 7, 0, 26, 0, 5),
(6, 'Southern Laquered Poussin', 'dirty rice, malt vinegar gastrique', '2014-11-18 19:13:22', '2014-11-18 19:29:05', 7, 0, 19, 0, 6),
(7, 'Fried Chicken and Waffles', 'choice of waffle: classic, bacon-cheddar, dried cherry, rosemary-mushroom, walnut-parmesan, or spiced-pecan', '2014-11-18 19:14:02', '2014-11-18 19:30:58', 7, 0, 16, 0, 7),
(8, 'Sweet Chick Bucket', '3 pc fried chicken, collard slaw, biscuit', '2014-11-18 19:14:26', '2014-11-18 19:29:13', 7, 0, 17, 0, 8),
(9, 'The General', 'fried chicken with general tsp''s sauce, rice and broccoli waffle', '2014-11-18 19:15:00', '2014-11-18 19:29:16', 7, 0, 17, 0, 9),
(10, 'Buffalo Fried Chicken', 'celery and carrot waffle, blue cheese', '2014-11-18 19:15:27', '2014-11-18 19:29:21', 7, 0, 17, 0, 10),
(11, 'Fried Chicken Parmesan', 'tomato sauce, mozzarella, sun dried tomato and basil waffle', '2014-11-18 19:15:49', '2014-11-18 19:29:24', 7, 0, 17, 0, 11),
(12, 'Blackened Chicken and Waffle', 'grits waffle', '2014-11-18 19:16:28', '2014-11-18 19:29:33', 7, 0, 17, 0, 12),
(13, 'Cordon Blue Chicken and Waffle', 'gruyere, thyme waffle', '2014-11-18 19:16:50', '2014-11-18 19:29:37', 7, 0, 17, 0, 13),
(14, 'Mike''s Hot Honey Chicken and Waffle', 'spiced honey', '2014-11-18 19:17:10', '2014-11-18 19:29:39', 7, 0, NULL, 0, 14),
(15, 'Chicken and Waffle of the Day', '', '2014-11-18 19:17:27', '2014-11-18 19:32:21', 7, 0, 17, 0, 15),
(16, 'Kale B.L.T.', 'house sured bacon, tomatoes, preserved lemon vinaigrette', '2014-11-18 19:32:47', '2014-11-18 19:32:47', 7, 0, 12, 1, 16),
(17, 'Mac and Cheese', 'gruyere, fontina, aged white cheddar, ritz cracker crust', '2014-11-18 19:34:59', '2014-11-18 19:34:59', 7, 0, 8, 1, 17),
(18, 'Duck Liver Pate', 'on toast with sea salt, spiced honey', '2014-11-18 19:35:23', '2014-11-18 19:35:23', 7, 0, 9, 1, 18),
(19, 'Sloppy Duck Sliders', 'three brioche rolls, sliced pickle', '2014-11-18 19:36:02', '2014-11-18 19:36:02', 7, 0, 9, 1, 19),
(20, 'BBQ Pork Sliders', 'house smoked pork, three brioche rolls, sliced pickles', '2014-11-18 19:36:39', '2014-11-18 19:36:39', 7, 0, 9, 1, 20),
(21, 'Braised Pork Belly', 'blueberry glaze, pickled watermelon, sesame seeds', '2014-11-18 19:37:07', '2014-11-18 19:37:07', 7, 0, 11, 1, 21),
(22, 'Fried Chicken and Pimento Cheese Sliders', '', '2014-11-18 19:37:28', '2014-11-18 19:37:28', 7, 0, 9, 1, 22),
(23, 'Crawfish Hush Puppies', 'remoulade sauce', '2014-11-18 19:37:51', '2014-11-18 19:37:51', 7, 0, 9, 1, 23),
(24, 'White Chicken Chili', 'sour cream, fontina cheese', '2014-11-18 19:38:16', '2014-11-18 19:38:16', 7, 0, 10, 1, 24),
(25, 'Baby Beat Salad', 'ricotta and parmesan fritters, honied pistachios, apples', '2014-11-18 19:38:51', '2014-11-18 19:38:51', 7, 0, 11, 1, 25),
(26, 'Braised Pork Belly', 'black eyed pea puree, crispy pickled jalapeños', '2014-11-18 19:39:40', '2014-11-18 19:39:40', 7, 0, 12, 1, 26),
(27, 'Sauteed Duck Hearts', 'roasted garlic grits, blue cheese crumbles, caramelized fennel', '2014-11-18 19:40:03', '2014-11-18 19:40:03', 7, 0, 12, 1, 27),
(28, 'Oyster Artichoke Dip', 'rockefeller style, grilled bread', '2014-11-18 19:40:24', '2014-11-18 19:40:24', 7, 0, 10, 1, 28),
(29, 'Collard Slaw', '', '2014-11-18 19:40:49', '2014-11-18 19:40:49', 7, 1, 5, 0, 29),
(30, 'Seasonal Market Vegetables', '', '2014-11-18 19:41:09', '2014-11-18 19:41:09', 7, 1, 7, 0, 30),
(31, 'Daily Cornbread', '', '2014-11-18 19:41:28', '2014-11-18 19:41:28', 7, 1, 2, 0, 31),
(32, 'Buttermilk Biscuit', '', '2014-11-18 19:41:46', '2014-11-18 19:41:46', 7, 1, 2.5, 0, 32);

-- --------------------------------------------------------

--
-- Table structure for table `meals`
--

CREATE TABLE IF NOT EXISTS `meals` (
`id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `menu_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `description` text,
  `position` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `meals`
--

INSERT INTO `meals` (`id`, `title`, `menu_id`, `created_at`, `updated_at`, `description`, `position`) VALUES
(5, 'Lunch', 1, '2014-11-18 01:35:21', '2014-11-18 18:25:10', 'LUNCH mon-fri 11am-4pm', 1),
(6, 'Brunch', 1, '2014-11-18 01:54:25', '2014-11-19 21:12:54', 'BRUNCH 10am-4pm Sat and Sun', 5),
(7, 'Dinner', 1, '2014-11-18 01:54:51', '2014-11-18 03:09:12', 'DINNER everyday 6pm-2am', 3),
(8, 'Dessert', 1, '2014-11-18 01:55:25', '2014-11-18 18:24:33', 'DESSERT $9', 4),
(9, 'In-Between', 1, '2014-11-18 01:56:26', '2014-11-19 21:12:25', 'IN-BETWEEN MENU 4pm-6pm everyday', 2),
(10, 'Lunch', 2, '2014-11-19 22:27:27', '2014-11-19 22:27:27', 'LUNCH mon-fri 11am-4pm', 1),
(11, 'In-Between', 2, '2014-11-19 22:28:11', '2014-11-19 22:28:11', 'IN-BETWEEN MENU 4pm-6pm everyday', 2),
(12, 'Dinner', 2, '2014-11-19 22:28:47', '2014-11-19 22:28:47', 'DINNER everyday 6pm-2am', 3),
(13, 'Dessert', 2, '2014-11-19 22:29:29', '2014-11-19 22:29:29', 'DESSERT $9', 4),
(14, 'Brunch', 2, '2014-11-19 22:29:57', '2014-11-19 22:29:57', 'BRUNCH 10am-4pm Sat and Sun', 5);

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE IF NOT EXISTS `menus` (
`id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'Lower East Side', '2014-11-17 23:35:33', '2014-11-17 23:35:33'),
(2, 'Brooklyn', '2014-11-18 01:38:24', '2014-11-18 01:38:24');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dishes`
--
ALTER TABLE `dishes`
 ADD PRIMARY KEY (`id`), ADD KEY `index_dishes_on_meal_id` (`meal_id`);

--
-- Indexes for table `meals`
--
ALTER TABLE `meals`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dishes`
--
ALTER TABLE `dishes`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `meals`
--
ALTER TABLE `meals`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
