-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 18, 2014 at 04:06 PM
-- Server version: 10.0.14-MariaDB-log
-- PHP Version: 5.6.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sweetchick_development`
--

--
-- Dumping data for table `dishes`
--

INSERT INTO `dishes` (`id`, `title`, `description`, `created_at`, `updated_at`, `meal_id`, `is_side`, `price`, `is_starter`) VALUES
(1, 'Vegetarian Meatloaf', 'pomme puree, peas, carrots', '2014-11-18 03:10:01', '2014-11-18 03:10:44', 7, 0, 22, 0);

--
-- Dumping data for table `galleries`
--

INSERT INTO `galleries` (`id`, `title`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Main Gallery', '', '2014-11-17 23:40:35', '2014-11-17 23:40:35');

--
-- Dumping data for table `meals`
--

INSERT INTO `meals` (`id`, `title`, `menu_id`, `created_at`, `updated_at`, `description`) VALUES
(5, 'Lunch', 1, '2014-11-18 01:35:21', '2014-11-18 01:35:21', NULL),
(6, 'Brunch', 1, '2014-11-18 01:54:25', '2014-11-18 01:54:25', NULL),
(7, 'Dinner', 1, '2014-11-18 01:54:51', '2014-11-18 03:09:12', 'DINNER everyday 6pm-2am'),
(8, 'Dessert', 1, '2014-11-18 01:55:25', '2014-11-18 01:55:25', NULL),
(9, 'In-Between', 1, '2014-11-18 01:56:26', '2014-11-18 01:56:26', NULL);

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'Lower East Side', '2014-11-17 23:35:33', '2014-11-17 23:35:33'),
(2, 'Brooklyn', '2014-11-18 01:38:24', '2014-11-18 01:38:24');

--
-- Dumping data for table `photos`
--

INSERT INTO `photos` (`id`, `image_uid`, `image_name`, `caption`, `created_at`, `updated_at`, `gallery_id`) VALUES
(1, '2014/11/17/g409tx2c8_ghzratx1aya0tkbtvkfc.jpg', 'ghzratx1aya0tkbtvkfc.jpg', 'blake', '2014-11-17 23:41:10', '2014-11-17 23:41:10', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
