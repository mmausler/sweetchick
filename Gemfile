source 'https://rubygems.org'


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.1.8'
# Use mysql as the database for Active Record
gem 'mysql2'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.3'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.0.0'
# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer',  platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'

# Use remotipart for async image upload
gem 'remotipart', '~> 1.2'

# Use jQuery File Upload plugin for multiple file uploads
gem 'jquery-fileupload-rails', github: 'Springest/jquery-fileupload-rails'

# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
gem 'jpbuilder'

# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0',          group: :doc

# Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
# gem 'spring',        group: :development

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
gem 'capistrano-rails'
gem 'capistrano-bundler'
gem 'capistrano-rvm'
gem 'capistrano-passenger'

# Use debugger
# gem 'debugger', group: [:development, :test]

# Use HAML for views
gem 'haml'
gem 'haml-rails'

# Use compass for SASS utils
gem 'compass-rails'
gem 'compass'

# Use Bootstrap with SASS support and Autoprefixer
gem 'bootstrap-sass'
gem 'autoprefixer-rails'

# Use devise for Auth
gem 'devise'

# Use ActiveAdmin
gem 'activeadmin', github: 'activeadmin'
gem 'active_skin'

# Use chosen to spice up ActiveAdmin interface
gem 'chosen-rails'

# Use ActsAsList and Active Admin Sortable to allow re-ordering of Dishes
gem 'acts_as_list'
gem 'activeadmin-sortable'

# Use Dragonfly for image uploading
gem 'dragonfly'
gem 'activeadmin-dragonfly', github: 'rizidoro/activeadmin-dragonfly'

# Use mailchimp for email list
gem 'mailchimp-api', require: 'mailchimp'

# Use reCaptcha for holler form
gem "recaptcha", :require => "recaptcha/rails"

# Use Stripe for payment processing
gem 'stripe', :git => 'https://github.com/stripe/stripe-ruby'

# Enable CORS
gem 'rack-cors', :require => 'rack/cors'

# Use rake-cache for caching on Prod
group :production do
    gem 'rack-cache', :require => 'rack/cache'

end
