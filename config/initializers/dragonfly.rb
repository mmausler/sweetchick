require 'dragonfly'

# Configure
Dragonfly.app.configure do
  plugin :imagemagick

  secret "1b77c66d339c1198c9d76c65e75a2ebe48830735ac9a7fcb1f3dfab78f1fb641"

  url_format "/media/:job/:name"

  datastore :file,
    root_path: Rails.root.join('public/uploads/photos'),
    server_root: Rails.root.join('public')
end

# Logger
Dragonfly.logger = Rails.logger

# Mount as middleware
Rails.application.middleware.use Dragonfly::Middleware

# Add model functionality
if defined?(ActiveRecord::Base)
  ActiveRecord::Base.extend Dragonfly::Model
  ActiveRecord::Base.extend Dragonfly::Model::Validations
end
