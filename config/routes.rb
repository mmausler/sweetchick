Rails.application.routes.draw do

  # Set up staging URLs
  if Rails.env.staging?
    constraints :subdomain => "api.staging" do
      scope :module => "api", :as => "api" do
        get 'menus', to: 'menus#index'
        get 'photos', to: 'photos#index'
        get 'press', to: 'press#index'
        get 'pages', to: 'pages#index'
        get 'employees', to: 'employees#index'
      end
    end
    constraints :subdomain => "admin.staging" do
        #ActiveAdmin.routes(self)
        ActiveAdmin.routes(self) rescue ActiveAdmin::DatabaseHitDuringLoad
        devise_for :admin_users, ActiveAdmin::Devise.config
    end
  # Set up production subdomains
  elsif Rails.env.production? || Rails.env.development?
    constraints :subdomain => "api" do
      scope :module => "api", :as => "api" do
        get 'menus', to: 'menus#index'
        get 'photos', to: 'photos#index'
        get 'press', to: 'press#index'
        get 'pages', to: 'pages#index'
        get 'employees', to: 'employees#index'
      end
    end
    constraints :subdomain => "admin" do
        #ActiveAdmin.routes(self)
        ActiveAdmin.routes(self) rescue ActiveAdmin::DatabaseHitDuringLoad
        devise_for :admin_users, ActiveAdmin::Devise.config
    end
  end


  # You can have the root of your site routed with "root"
  #root 'home#index'

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
