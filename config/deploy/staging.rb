# Simple Role Syntax
# ==================
# Supports bulk-adding hosts to roles, the primary server in each group
# is considered to be the first unless any hosts have the primary
# property set.  Don't declare `role :all`, it's a meta role.

set :application, 'sweetchick_staging'

role :app, %w{dev@sweetchicknyc.com}
role :web, %w{dev@sweetchicknyc.com}
role :db,  %w{dev@sweetchicknyc.com}


set :deploy_to, '/var/www/staging.sweetchicknyc.com'

#set :rvm_ruby_version, '2.2.0@sweetchick'

# set :bundle_path, -> { '/home/dev/.rvm/gems/ruby-2.2.0@sweetchick/bin/bundle' }


# Extended Server Syntax
# ======================
# This can be used to drop a more detailed server definition into the
# server list. The second argument is a, or duck-types, Hash and is
# used to set extended properties on the server.

#server 'example.com', user: 'deploy', roles: %w{web app}, my_property: :my_value


# Custom SSH Options
# ==================
# You may pass any option but keep in mind that net/ssh understands a
# limited set of options, consult[net/ssh documentation](http://net-ssh.github.io/net-ssh/classes/Net/SSH.html#method-c-start).
#
# Global options
# --------------
#  set :ssh_options, {
#    keys: %w(/home/rlisowski/.ssh/id_rsa),
#    forward_agent: false,
#    auth_methods: %w(password)
#  }
#
# And/or per server (overrides global)
# ------------------------------------
 server 'sweetchicknyc.com',
   user: 'dev',
   roles: %w{web app},
   keys: %w(/home/michael/.ssh/id_dsa_digitalocean),
   port: 6688,
   forward_agent: false,
   auth_methods: %w(publickey)
   # password: 'please use keys'
