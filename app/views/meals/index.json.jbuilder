json.meals @meals do |meal|
  json.title meal.title
  json.dishes meal.dishes do |dish|
    json.title dish.title
  end
end
