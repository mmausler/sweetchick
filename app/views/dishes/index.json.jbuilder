json.dishes @dishes do |dish|
  json.title dish.title
  json.description dish.description
  json.price dish.price
end
