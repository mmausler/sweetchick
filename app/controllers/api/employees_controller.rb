class Api::EmployeesController < ApplicationController

  def new
    @employee = Employee.order('created_at DESC')
    @employee = Employee.new
  end

  def create
    respond_to do |format|
      @employee = employee.all
      @employee = employee.new(employee_params)
      @employee.save
    end
  end

  def index
    @employees = Employee.all
  end

  def employee_params
    params.require(:employee).permit(:name, :role, :description, :image)
  end
end

