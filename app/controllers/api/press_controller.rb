class Api::PressController < ApplicationController

  def new
    @press = Press.order('created_at DESC')
    @press = Press.new
  end

  def create
    respond_to do |format|
      @press = Press.all
      @press = Press.new(press_params)
      @press.save
    end
  end

  def index
    @press = Press.all
  end

  def press_params
    params.require(:press).permit(:title, :description, :image, :release_date, :embed_code, :url)
  end
end
