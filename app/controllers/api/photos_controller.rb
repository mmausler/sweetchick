class Api::PhotosController < ApplicationController
  def new
    @photos = Photo.order('created_at DESC')
    @galleries = Gallery.all
    @photo = Photo.new
  end
 
  def create
    respond_to do |format|
      @photos = Photo.all
      @photo = Photo.new(photo_params)
      @photo.save
      format.html {redirect_to(:action => :index, :notice => 'Photo successfully created')}
      format.js {}
    end
  end

  def new_multiple
    @galleries = Gallery.all
    @photos = Photo.order('created_at DESC')
    @photo = Photo.new
  end

  def index
    @photos = Photo.all
  end
 
  private
 
  def photo_params
    params.require(:photo).permit(:image, :caption, :gallery_id)
  end
end
