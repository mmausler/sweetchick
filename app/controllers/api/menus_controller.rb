class Api::MenusController < ApplicationController

  def new
  end

  def create
    @menu = Menu.new(menu_params)

    @menu.save
    redirect_to @menu
  end

  def show
    @menu = Menu.find(params[:id])
  end

  def index
      @menus = Menu.all
  end

  private
    def menu_params
      params.required(:menu).permit(:title,:description)
    end

end
