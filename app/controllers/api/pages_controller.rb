class PageController < ApplicationController

  def new
    @page = Page.order('created_at DESC')
    @page = Page.new
  end

  def create
    respond_to do |format|
      @page = page.all
      @page = page.new(page_params)
      @page.save
    end
  end

  def index
    @page = page.all
  end

  def page_params
    params.require(:page).permit(:title, :body)
  end
end
