class Menu < ActiveRecord::Base
  has_many :meals, ->{ order(:position)}
  validates :title, presence: true
end
