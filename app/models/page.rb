class Page < ActiveRecord::Base
  validates :title, presence: true
  before_save      Page.new

  def before_save(page)
    Rails::logger.info
    page.body = ActionController::Base.helpers.sanitize(page.body)
  end
end
