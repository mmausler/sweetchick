class Dish < ActiveRecord::Base
  belongs_to :meal
  validates :title, presence: true
  acts_as_list scope: :meal
  #default_scope {:order => 'dishes.position'}
end
