class Photo < ActiveRecord::Base
  belongs_to :gallery
  accepts_nested_attributes_for :gallery
  default_scope { order(position: :asc) }
  acts_as_list
  dragonfly_accessor :image
  validates :image, presence: true
  validates_size_of :image, maximum: 2.megabytes,
    message: "should be no more than 500 KB", if: :image_changed?
  validates_property :format, of: :image, in: [:jpeg, :jpg, :png, :bmp], case_sensitive: false,
    message: "should be either .jpeg, .jpg, .png, .bmp", if: :image_changed?
end
