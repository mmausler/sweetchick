class Meal < ActiveRecord::Base
  belongs_to :menu
  accepts_nested_attributes_for :menu
  has_many :dishes, ->{ order(:position)}
  acts_as_list scope: :menu
  validates :title, presence: true
end

