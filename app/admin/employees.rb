ActiveAdmin.register Employee do
  permit_params :name, :role, :description, :image, :retained_image, :subdomain

  config.filters = false
  config.sort_order = 'position_asc'
  config.paginate   = false
  sortable
  form :html => { :enctype => "multipart/form-data" } do |f|
    inputs do
      input :name
      input :role
      input :description
      input :image, as: :dragonfly
    end
    actions
  end
  index do |employee|
    sortable_handle_column
    column :name, :sortable => false
    column :role, :sortable => false
    column :description, :sortable => false
    column :image, :sortable => false
    column "Preview" do |employee|
      if employee.image
        link_to image_tag(employee.image.thumb('80x50#').url, class: 'img-thumbnail'),
        employee.image.remote_url, target: '_blank'
      end
    end
    actions
  end
end

