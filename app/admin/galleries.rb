ActiveAdmin.register Gallery do
  permit_params :title, :description
  menu false
end
