ActiveAdmin.register Page do
  permit_params :title, :body

  config.filters = false
  index do |page|
    column "Title" do |title|
      link_to page.title, page_path(page)
    end
    column :body, :sortable => false
    actions
  end
end

