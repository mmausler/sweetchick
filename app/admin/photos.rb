ActiveAdmin.register Photo do
  permit_params :image, :image_name, :caption, :gallery_id
  form :partial => "form"
  config.filters = false
  config.sort_order = 'position_asc'
  config.paginate   = false
  sortable
  index do |photo|
    sortable_handle_column
    column "Preview" do |photo|
      link_to image_tag(photo.image.thumb('80x50#').url, alt: photo.caption, class: 'img-thumbnail'),
      photo.image.remote_url, target: '_blank'
    end
    column :image_name, :sortable => false
    actions
  end
end
