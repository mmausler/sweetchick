ActiveAdmin.register Meal do
  permit_params :title, :description, :menu_id
  belongs_to :menu 
  config.sort_order = 'position_asc'
  config.paginate   = false
  sortable

  sidebar :meal, only: [:show, :edit] do
    ul do
      li link_to "Dishes", meal_dishes_path(meal)
    end
  end
  index do |dish|
    sortable_handle_column
    column "Title" do |meal|
      link_to meal.title, meal_dishes_path(meal)
    end
    column :description, :sortable => false
    actions
  end
end

