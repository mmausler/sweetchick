ActiveAdmin.register Menu do
  permit_params :title

  config.filters = false
  index do |menu|
    column "Title" do |menu|
      link_to menu.title, menu_meals_path(menu)
    end
    column :description, :sortable => false
    actions
  end
end
