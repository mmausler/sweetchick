ActiveAdmin.register Dish do
  permit_params :title, :description, :meal_id, :price, :is_starter, :is_side
  belongs_to :meal
  config.sort_order = 'position_asc'
  config.paginate   = false
  sortable
  index do |dish|
    sortable_handle_column
    column :title, :sortable => false
    column :description, :sortable => false
    column :price, :sortable => false
    column :is_starter, :sortable => false
    column :is_side, :sortable =>false
    actions
  end
end
