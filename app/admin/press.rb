ActiveAdmin.register Press do
  permit_params :title, :description, :embed_code, :url, :image, :retained_image, :release_date

  config.filters = false
  config.sort_order = 'position_asc'
  config.paginate   = false
  sortable
  form :html => { :enctype => "multipart/form-data" } do |f|
    inputs do
      input :title
      input :description
      input :release_date, as: :datepicker
      input :embed_code
      input :url
      input :image, as: :dragonfly
    end
    actions
  end
  index do |press|
    sortable_handle_column
    column :title, :sortable => false
    column :description, :sortable => false
    column "Preview" do |press|
      if press.image
        link_to image_tag(press.image.thumb('80x50#').url, class: 'img-thumbnail'),
        press.image.remote_url, target: '_blank'
      end
    end
    column :embed_code, :sortable => false
    actions
  end
end
